CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module creates a user for each role on a site. The user's password is
always the same as the name of the role.

Security
This module is only ever designed to be run on development sites.
Don't enable this module on production or any other site which doesn't use other
means for restricting access.


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Dave Hall (skwashd) - https://www.drupal.org/user/116305
 * Natan Moraes (natanmoraes) - https://www.drupal.org/user/1902710
